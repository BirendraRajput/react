﻿import React, { Component } from 'react';

export class MyPage extends Component {   
    displayName = MyPage.name

    constructor(props) {
        super(props);
        this.state = { result: [], loading: true };

        fetch('api/SampleData/WeatherList')
            .then(response => response.json())
            .then(data => {
                this.setState({ results: data, loading: false });
            });
    }

    static renderForecastsDropdown(results) {
        return (
            <select>
                <option value=''>Select</option>
                {results.map(result =>
                    <option value={result}>{result}</option>
                )}
            </select>
        );
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : MyPage.renderForecastsDropdown(this.state.results);
        return (
            <div>
                <p>This is components demonstration</p>
                {contents}
            </div>
        );
    }

}